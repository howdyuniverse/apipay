<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ChargeTransactions;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(ChargeTransactions::class, function (Faker $faker) {
    return [
        'created_at' => ChargeTransactions::getTimestampDatetime(),
        'updated_at' => ChargeTransactions::getTimestampDatetime(),
        'user_id' => factory(User::class)->create(),
        'reference_id' => Str::random(10),
        'amount' => '100.0',
    ];
});
