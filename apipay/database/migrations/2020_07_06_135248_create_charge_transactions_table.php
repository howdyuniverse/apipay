<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChargeTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charge_transactions', function (Blueprint $table) {
            $table->id();
            $table->timestamp('created_at', 4)->useCurrent();
            $table->timestamp('updated_at', 4)->useCurrent();
            $table->foreignId('user_id');
            $table->string('reference_id', 36)->unique();
            $table->string('type', 100);
            $table->decimal('amount', 10, 2);
            $table->string('status', 100);

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charge_transactions');
    }
}
