<?php

namespace Tests\Feature\Jobs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

use App\ChargeTransactions;
use App\Jobs\TopupBalance;
use App\Libs\ExactlyClient;
use App\User;

class TopupBalanceJobTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_topup_user_balance_for_successful_charge()
    {
        $referenceId = 'test-reference-1';
        $status = ExactlyClient::STATUS_SUCCESSFUL;
        $oldUserBalance = '100.00';
        $topupAmount = '99.9';
        $user = factory(User::class)->create(['balance' => $oldUserBalance]);
        factory(ChargeTransactions::class)->create([
            'user_id' => $user->id,
            'reference_id' => $referenceId,
            'type' => ChargeTransactions::TYPE_TOPUP,
            'status' => ChargeTransactions::STATUS_PENDING,
            'amount' => $topupAmount,
        ]);

        TopupBalance::dispatchNow($referenceId, $status);

        $this->assertEquals(
            bcadd($oldUserBalance, $topupAmount, 2),
            User::where('id', $user->id)->first()->balance
        );
        $this->assertEquals(
            ChargeTransactions::STATUS_SUCCESSFUL,
            ChargeTransactions::where('reference_id', $referenceId)
                ->first()->status
        );
    }

    /** @test */
    public function it_set_status_failed_for_failed_charge_transaction()
    {
        $referenceId = 'test-reference-1';
        $status = ExactlyClient::STATUS_FAILED;
        factory(ChargeTransactions::class)->create([
            'reference_id' => $referenceId,
            'type' => ChargeTransactions::TYPE_TOPUP,
            'status' => ChargeTransactions::STATUS_PENDING,
        ]);

        TopupBalance::dispatchNow($referenceId, $status);

        $this->assertEquals(
            ChargeTransactions::STATUS_FAILED,
            ChargeTransactions::where('reference_id', $referenceId)
                ->first()->status
        );
    }

    /** @test */
    public function it_do_nothing_on_unexpected_api_status()
    {
        $referenceId = 'test-reference-1';
        $status = ExactlyClient::STATUS_INITIALIZED;
        $oldUserBalance = '100.00';
        $topupAmount = '99.9';
        $user = factory(User::class)->create(['balance' => $oldUserBalance]);
        factory(ChargeTransactions::class)->create([
            'user_id' => $user->id,
            'reference_id' => $referenceId,
            'type' => ChargeTransactions::TYPE_TOPUP,
            'status' => ChargeTransactions::STATUS_PENDING,
            'amount' => $topupAmount,
        ]);

        TopupBalance::dispatchNow($referenceId, $status);

        $this->assertEquals(
            $oldUserBalance,
            User::where('id', $user->id)->first()->balance
        );
        $this->assertEquals(
            ChargeTransactions::STATUS_PENDING,
            ChargeTransactions::where('reference_id', $referenceId)
                ->first()->status
        );
    }
}
