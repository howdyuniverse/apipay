<?php

namespace Tests\Feature\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\User;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSuccessfulLogin()
    {
        $name = 'alice';
        $email = $name .'@test.com';
        $password = '1234';
        factory(User::class)->create([
            'name' => $name,
            'password' => bcrypt($password),
            'email' => $email,
            'balance' => '0.0',
        ]);
        $apiResponse = $this->json(
            'POST', 'api/auth/login',
            ['email' => $email, 'password' => $password]
        );
        $apiResponse->assertStatus(200);
        $this->assertTrue(isset($apiResponse['access_token']));
    }

    public function testUnsuccessfulLogin()
    {
        $name = 'alice';
        $email = $name .'@test.com';
        $password = '1234';
        factory(User::class)->create([
            'name' => $name,
            'password' => bcrypt($password),
            'email' => $email,
            'balance' => '0.0',
        ]);
        $apiResponse = $this->json(
            'POST', 'api/auth/login',
            ['email' => $email, 'password' => 'bad'. $password]
        );
        $apiResponse->assertStatus(401);
    }
}
