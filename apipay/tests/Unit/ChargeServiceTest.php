<?php

namespace Tests\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Queue;

use App\Apipay\States\Charge\SuccessfulChargeState;
use App\Apipay\States\Charge\RedirectChargeState;
use App\Apipay\States\Charge\FailedChargeState;
use App\User;
use App\ChargeTransactions;
use App\Apipay\Repositories\ChargeRepository;
use App\Apipay\Strategies\Charge\ChargeTransactionInitDirectStrategy;
use App\Apipay\Strategies\Charge\ChargeTransactionInitStrategy;
use App\Libs\ExactlyClient;
use App\Services\ChargeService;
use App\Jobs\TopupBalance;
use Illuminate\Http\JsonResponse;

class ChargeServiceTest extends TestCase
{
    use DatabaseTransactions;

    protected $fakeApiClient;
    
    public const REDIRECT_URI = 'https://endpoint.example.com';
    
    protected function setUp(): void
    {
        parent::setUp();

        $this->fakeApiResponse = new FakeExactlyClientResponse();
    }

    private function init_direct_charges_test(
        string $apiResponseStatus,
        JsonResponse $expectedReponse,
        string $expectedChargeTransactionStatus
    ) {
        $userName = 'Alice';
        $userEmail = $userName . '@test.com';
        $clientIP = '127.0.0.1';
        $user = factory(User::class)->create([
            'name' => $userName,
            'email' => $userEmail,
            'balance' => '0.00',
        ]);

        $amount = '100.00';
        $cardNumber = '1111222233334444';
        $referenceId = 'reference-id-123';
        $stubResponse = $this->fakeApiResponse->get_api_charges(
            $referenceId,
            $apiResponseStatus,
            $amount,
            $userEmail,
            $userName,
            $cardNumber
        );
        $apiClientStub = $this->getMockBuilder(ExactlyClient::class)->getMock();
        $apiClientStub->method('directCharges')
            ->willReturn($stubResponse);
        $chargeRepo = new ChargeRepository();
        $chargeService = new ChargeService($chargeRepo, $apiClientStub);
        $transactionData = [
            'client_ip' => $clientIP,
            'amount' => $amount,
            'name' => $userName,
            'card_number' => $cardNumber,
            'card_expiry_month' => 4,
            'card_expiry_year' => date('Y') + 1,
            'card_csc' => 111,
            'reference_id' => $referenceId,
        ];
        $state = $chargeService->initChargeTransaction(
            $user,
            $transactionData,
            ChargeTransactions::TYPE_TOPUP,
            new ChargeTransactionInitDirectStrategy($chargeRepo, $apiClientStub)
        );

        $this->assertEquals(
            $expectedReponse,
            $state->response()
        );
        $this->assertEquals(
            $expectedChargeTransactionStatus,
            ChargeTransactions::where('reference_id', $referenceId)
                ->first()->status
        );
    }

    private function init_charges_test(
        string $apiResponseStatus,
        JsonResponse $expectedReponse,
        string $expectedChargeTransactionStatus
    ) {
        $userName = 'Alice';
        $userEmail = $userName . '@test.com';
        $user = factory(User::class)->create([
            'name' => $userName,
            'email' => $userEmail,
            'balance' => '0.00',
        ]);

        $amount = '100.00';
        $cardNumber = null;
        $referenceId = 'reference-id-123';
        $stubResponse = $this->fakeApiResponse->get_api_charges(
            $referenceId,
            $apiResponseStatus,
            $amount,
            $userEmail,
            $userName,
            $cardNumber
        );
        $apiClientStub = $this->getMockBuilder(ExactlyClient::class)->getMock();
        $apiClientStub->method('charges')
            ->willReturn($stubResponse);
        $chargeRepo = new ChargeRepository();
        $chargeService = new ChargeService($chargeRepo, $apiClientStub);
        $state = $chargeService->initChargeTransaction(
            $user,
            [
                'amount' => $amount,
                'reference_id' => $referenceId,
            ],
            ChargeTransactions::TYPE_TOPUP,
            new ChargeTransactionInitStrategy($chargeRepo, $apiClientStub)
        );

        $this->assertEquals(
            $expectedReponse,
            $state->response()
        );
        $this->assertEquals(
            $expectedChargeTransactionStatus,
            ChargeTransactions::where('reference_id', $referenceId)
                ->first()->status
        );
    }

    /** @test */
    public function it_initializes_topup_direct_charge_transaction_without_3d_secure()
    {
        $this->init_direct_charges_test(
            ExactlyClient::STATUS_SUCCESSFUL,
            SuccessfulChargeState::get()->response(),
            ChargeTransactions::STATUS_PENDING,
        );
    }

    /** @test */
    public function it_initializes_topup_direct_charge_transaction_with_3d_secure()
    {
        $this->init_direct_charges_test(
            ExactlyClient::STATUS_PENDING,
            RedirectChargeState::get(
                ['redirect_uri' => ChargeServiceTest::REDIRECT_URI]
            )->response(),
            ChargeTransactions::STATUS_PENDING,
        );
    }

    /** @test */
    public function it_failed_topup_direct_charge_transaction()
    {
        $this->init_direct_charges_test(
            ExactlyClient::STATUS_FAILED,
            FailedChargeState::get()->response(),
            ChargeTransactions::STATUS_FAILED,
        );
    }

    /** @test */
    public function it_initialized_topup_charge_transaction()
    {
        $this->init_charges_test(
            ExactlyClient::STATUS_INITIALIZED,
            RedirectChargeState::get(
                ['redirect_uri' => ChargeServiceTest::REDIRECT_URI]
            )->response(),
            ChargeTransactions::STATUS_PENDING
        );
    }

    /** @test */
    public function it_failed_topup_charge_transaction()
    {
        $this->init_charges_test(
            ExactlyClient::STATUS_FAILED,
            FailedChargeState::get()->response(),
            ChargeTransactions::STATUS_FAILED
        );
    }

    private function enqueue_charge_callback_event_test(
        string $referenceId,
        string $chargeTransactionType,
        string $chargeTransactionStatus,
        string $apiResponseStatus
    )
    {
        $amount = '100.00';
        $transaction = factory(ChargeTransactions::class)->create([
            'reference_id' => $referenceId,
            'type' => $chargeTransactionType,
            'amount' => $amount,
            'status' => $chargeTransactionStatus,
        ]);

        $stubResponse = $this->fakeApiResponse->get_api_charges(
            $referenceId,
            $apiResponseStatus,
            $amount,
            $transaction->user->email,
            $transaction->user->name,
        );
        $apiClientStub = $this->getMockBuilder(ExactlyClient::class)->getMock();
        $apiClientStub->method('getCharge')
            ->willReturn($stubResponse);
        $chargeService = new ChargeService(
            new ChargeRepository(),
            $apiClientStub,
        );
        $chargeService->enqueueChargeCallbackEvent(
            ['reference_id' => $referenceId]
        );
    }

    /** @test */
    public function it_enqueued_topup_balance_job_for_successful_charge()
    {
        Queue::fake();
        $this->enqueue_charge_callback_event_test(
            'test-reference',
            ChargeTransactions::TYPE_TOPUP,
            ChargeTransactions::STATUS_PENDING,
            ExactlyClient::STATUS_SUCCESSFUL,
        );
        Queue::assertPushed(TopupBalance::class);
    }

    /** @test */
    public function it_enqueued_topup_balance_for_failed_charge()
    {
        Queue::fake();
        $this->enqueue_charge_callback_event_test(
            'test-reference',
            ChargeTransactions::TYPE_TOPUP,
            ChargeTransactions::STATUS_PENDING,
            ExactlyClient::STATUS_FAILED,
        );
        Queue::assertPushed(TopupBalance::class);
    }

    /** @test */
    public function it_not_enqueued_topup_balance_for_not_pending_charge_transaction()
    {
        Queue::fake();
        $this->enqueue_charge_callback_event_test(
            'test-reference',
            ChargeTransactions::TYPE_TOPUP,
            ChargeTransactions::STATUS_SUCCESSFUL,
            ExactlyClient::STATUS_SUCCESSFUL,
        );
        Queue::assertNotPushed(TopupBalance::class);
    }

    /** @test */
    public function it_not_enqueued_topup_balance_job_without_existing_charge_transaction()
    {
        $apiResponseStatus = ExactlyClient::STATUS_SUCCESSFUL;

        $referenceId = 'test-reference';
        $stubResponse = $this->fakeApiResponse->get_api_charges(
            $referenceId,
            $apiResponseStatus,
        );
        $apiClientStub = $this->getMockBuilder(ExactlyClient::class)->getMock();
        $apiClientStub->method('getCharge')
            ->willReturn($stubResponse);
        $chargeService = new ChargeService(
            new ChargeRepository(),
            $apiClientStub,
        );
        Queue::fake();
        $chargeService->enqueueChargeCallbackEvent(
            ['reference_id' => $referenceId]
        );
        Queue::assertNotPushed(TopupBalance::class);
    }

    /** @test */
    public function it_not_enqueued_topup_balance_job_without_existing_api_charge()
    {
        $chargeTransactionType = ChargeTransactions::TYPE_TOPUP;
        $chargeTransactionStatus = ChargeTransactions::STATUS_PENDING;

        $callbackReferenceId = 'non-existsting-api-reference';
        $referenceId = 'test-reference';
        factory(ChargeTransactions::class)->create([
            'reference_id' => $referenceId,
            'type' => $chargeTransactionType,
            'status' => $chargeTransactionStatus,
        ]);

        $stubResponse = null;
        $apiClientStub = $this->getMockBuilder(ExactlyClient::class)->getMock();
        $apiClientStub->method('getCharge')
            ->willReturn($stubResponse);
        $chargeService = new ChargeService(
            new ChargeRepository(),
            $apiClientStub,
        );
        Queue::fake();
        $chargeService->enqueueChargeCallbackEvent(
            ['reference_id' => $callbackReferenceId]
        );
        Queue::assertNotPushed(TopupBalance::class);
    }
}


class FakeExactlyClientResponse
{
    public function get_api_charges(
        string $referenceId,
        string $status,
        string $amount = '100.00',
        string $email = 'alice@example.com',
        string $name = 'alice',
        ?string $cardNumber = '1111222233334444'
    ) {
        $response = [];
        $response['data'] = [
            'charge' => [
                'type' => 'charge',
                'id' => 'd5d0e700-8dd4-11e9-a7d7-c30351fce0d5',
            ]
        ];
        $response['data']['charge'] = [];
        $response['data']['charge']['attributes'] = [
            'livemode' => true,
            'status' => $status,
            'amount' => $amount,
            'paid_amount' => $amount,
            'fee' => '0.01',
            'rolling' => '1.00',
            'total_amount' => $amount,
            'currency' => 'USD',
            'pay_method' => 'card',
            'description' => 'Payment for invoice #00006',
            'secure_auth' => null,
            'failure' => null,
            'reference_id' => $referenceId,
            'created_at' => 1560428087,
            'updated_at' => 1560428087,
            'valid_till' => 1560446087,
        ];
        if ($cardNumber) {
            $response['data']['charge']['attributes']['source'] = [
                'email' => $email,
                'ip_address' => '127.0.0.1',
                'country_code' => 'US',
                'name' => $name,
                'card_number' => $cardNumber,
                'brand' => 'Visa',
                'issuer_country_code' => 'US',
                'issuer_name' => 'JPMORGAN CHASE BANK, N.A.',
            ];
        } else {
            $response['data']['charge']['attributes']['source'] = [
                'email' => $email,
            ];
        }
        if (
            $status == ExactlyClient::STATUS_PENDING
            || $status == ExactlyClient::STATUS_INITIALIZED) {
            $response['data']['links'] = [
                'redirect_uri' => ChargeServiceTest::REDIRECT_URI
            ];
        }
        $response['meta'] = [
            'server_time' => 1560428087,
            'server_timezone' => 'UTC',
            'api_version' => 'v2',
        ];
        
        return $response;
    }
}
