<?php

declare(strict_types=1);

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\ChargeTransactions;
use App\Apipay\Repositories\ChargeRepository;
use App\Libs\ExactlyClient;
use App\Services\ChargeService;

class TopupBalance implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $referenceId;
    protected $status;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $referenceId, string $status)
    {
        $this->referenceId = $referenceId;
        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        ChargeRepository $chargeRepo, ChargeService $chargeService
    ): void {
        switch ($this->status) {
            case ExactlyClient::STATUS_FAILED:
                $chargeRepo->updateChargeTransactionStatus(
                    $this->referenceId, ChargeTransactions::STATUS_FAILED
                );
                return;
            case ExactlyClient::STATUS_SUCCESSFUL:
                $chargeService->topupBalance($this->referenceId);
                return;
            default:
                logger(__CLASS__. ' unexpected status '. $this->referenceId);
        }
    }
}
