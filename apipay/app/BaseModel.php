<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public $timestamps = false;

    protected $dateFormat = 'Y-m-d H:i:s.u';

    public static function getTimestampDatetime()
    {
        return DateTime::createFromFormat('U.u', microtime(true))
            ->format('Y-m-d H:i:s.u');
    }
}
