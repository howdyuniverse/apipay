<?php

declare(strict_types=1);

namespace App;

use App\BaseModel;
use App\User;
use App\Libs\ExactlyClient;

class ChargeTransactions extends BaseModel
{
    public const STATUS_RECIEVED = 'recieved';
    public const STATUS_FAILED = 'failed';
    public const STATUS_SUCCESSFUL = 'successful';
    public const STATUS_PENDING = 'pending';

    public const TYPE_TOPUP = 'topup';

    protected $fillable = [
        'created_at',
        'updated_at',
        'user_id',
        'reference_id',
        'type',
        'amount',
        'status'
    ];

    public static function getModelStatusByApiStatus(string $apiStatus): string
    {
        $statusMapping = [
            ExactlyClient::STATUS_FAILED => ChargeTransactions::STATUS_FAILED,
            ExactlyClient::STATUS_SUCCESSFUL =>
                ChargeTransactions::STATUS_SUCCESSFUL,
            ExactlyClient::STATUS_PENDING => ChargeTransactions::STATUS_PENDING,
            ExactlyClient::STATUS_INITIALIZED =>
                ChargeTransactions::STATUS_PENDING,
        ];
        return $statusMapping[$apiStatus];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
