<?php

declare(strict_types=1);

namespace App\Services;

use Exception;
use DateTime;
use Illuminate\Support\Facades\DB;
use Recurr\Rule as RecurrRule;
use \Recurr\Transformer\ArrayTransformer as RecurrArrayTransformer;

use App\User;
use App\PaymentSchedule;
use App\ScheduledPayment;
use App\PaymentScheduleRecipients;
use App\Apipay\Repositories\PayRepository;
use App\Exceptions\PayException;
use App\Apipay\Enums\PayResultEnum;


class ScheduledPayService
{
    const SCHEDULED_TRANSACTIONS_BUCKET_SIZE = 10;
    const SCHEDULE_DATE_FORMAT = 'd/m/Y';

    public function __construct(PayRepository $payRepository)
    {
        $this->payRepository = $payRepository;
    }

    public function schedulePayments(
        User $user,
        array $scheduleData
    ) {
        $startDate = DateTime::createFromFormat(
            ScheduledPayService::SCHEDULE_DATE_FORMAT,
            $scheduleData['start_date'],
        );
        $endDate = DateTime::createFromFormat(
            ScheduledPayService::SCHEDULE_DATE_FORMAT,
            $scheduleData['end_date'],
        );
        $paymentSchedule = new PaymentSchedule();
        $paymentSchedule->sender_id = $user->id;
        $paymentSchedule->start_date = $startDate;
        $paymentSchedule->end_date = $endDate;
        $paymentSchedule->description = $scheduleData['description'];
        $paymentSchedule->is_active = true;
        $paymentSchedule->save();

        $scheduledRecipients = [];
        foreach ($scheduleData['recipients'] as $recipient) {
            $scheduledRecipients[] = [
                'recipient_id' => $recipient['id'],
                'payment_schedule_id' => $paymentSchedule->id,
                'amount' => $recipient['amount'],
            ];
        }
        PaymentScheduleRecipients::insert($scheduledRecipients);

        $rule = new RecurrRule('FREQ=WEEKLY', $startDate, $endDate);
        $rule->setUntil($endDate);
        $transformer = new RecurrArrayTransformer();
        $schedule = $transformer->transform($rule)->toArray();
        $scheduledPayments = [];
        foreach ($schedule as $scheduleItem) {
            $scheduledPayments[] = [
                'payment_schedule_id' => $paymentSchedule->id,
                'pay_date' => $scheduleItem->getStart(),
            ];
        }
        ScheduledPayment::insert($scheduledPayments);
    }

    public function doScheduledPayTransaction(
        ScheduledPayment $scheduledPayment
    ): int {
        $paymentSchedule = $scheduledPayment->paymentSchedule;
        $recipientsAmountsMap = [];
        foreach ($paymentSchedule->recipients as $recipient) {
            $recipientsAmountsMap[$recipient->recipient_id] = (
                $recipient->amount
            );
        }
        try {
            DB::beginTransaction();
            ScheduledPayment::where('id', $scheduledPayment->id)
                ->lockForUpdate();
            $this->payRepository->doPayWithoutTransaction(
                $paymentSchedule->sender_id,
                $recipientsAmountsMap,
            );
            ScheduledPayment::where('id', $scheduledPayment->id)
                ->update([
                    'paid_at' => ScheduledPayment::getTimestampDatetime()
                ]);
            DB::commit();
            return PayResultEnum::PAYMENT_OK;
        } catch (PayException $payException) {
            DB::rollBack();
            return $payException->getCode();
        } catch (Exception $e) {
            DB::rollBack();
            return PayResultEnum::ERR_CODE_INTERNAL;
        }
        return PayResultEnum::ERR_CODE_INTERNAL;
    }

    public function processReadyScheduledTransactions()
    {
        $now = new DateTime();
        $readyScheduledPayments = ScheduledPayment::with(
                'paymentSchedule.recipients'
            )
            ->where('pay_date', '<=', $now)
            ->whereNull('paid_at')
            ->limit(ScheduledPayService::SCHEDULED_TRANSACTIONS_BUCKET_SIZE)
            ->get();

        if (!count($readyScheduledPayments)) {
            print('No scheduled events');
            return;
        }
        foreach ($readyScheduledPayments as $scheduledPayment) {
            $resultCode = $this->doScheduledPayTransaction($scheduledPayment);
            switch ($resultCode) {
                case PayResultEnum::PAYMENT_OK:
                    print('user_id'. $scheduledPayment->payment_schedule_id .' OK');
                    break;
                case PayResultEnum::ERR_CODE_NOT_ENOUGH_MONEY:
                    print('user_id'. $scheduledPayment->payment_schedule_id .' not enough money');
                    // send email, sms etc...
                    // QQQ how to process these payments next time ?
                    break;
                case PayResultEnum::ERR_CODE_INTERNAL:
                    print('user_id'. $scheduledPayment->payment_schedule_id .' internal error');
                    break;
            }
        }
    }
}
