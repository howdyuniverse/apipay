<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\DB;

use App\User;
use App\ChargeTransactions;
use App\Libs\ExactlyClient;
use App\Jobs\TopupBalance;
use App\Http\Requests\BaseApiRequest;

use App\Apipay\Repositories\ChargeRepository;
use App\Apipay\States\Charge\ChargeState;
use App\Apipay\Strategies\Charge\ChargeTransactionInitInterface;

class ChargeService
{
    protected $chargeRepo;
    protected $exactlyClient;

    public function __construct(
        ChargeRepository $chargeRepo,
        ExactlyClient $exactlyClient
    ) {
        $this->chargeRepo = $chargeRepo;
        $this->exactlyClient = $exactlyClient;
    }

    public function topupBalance(string $referenceId)
    {
        DB::beginTransaction();
        $chargeTransaction = (
            ChargeTransactions::where('reference_id', $referenceId)
                ->lockForUpdate()
                ->first()
        );
        if ($chargeTransaction->status == ChargeTransactions::STATUS_SUCCESSFUL) {
            logger(__CLASS__ .' Interrupt successful transaction '. $referenceId);
            return;
        }
        User::where('id', $chargeTransaction->user_id)
            ->lockForUpdate()
            ->increment('balance', $chargeTransaction->amount);
        $this->chargeRepo->updateChargeTransactionStatus(
            $referenceId, ChargeTransactions::STATUS_SUCCESSFUL
        );
        DB::commit();
    }

    public function initChargeTransaction(
        User $user,
        array $transactionData,
        string $type,
        ChargeTransactionInitInterface $chargeStrategy
    ): ChargeState {
        return $chargeStrategy->init($user, $transactionData, $type);
    }

    public function enqueueChargeCallbackEvent(array $callbackData)
    {
        $referenceId = $callbackData['reference_id'];
        $charge = $this->exactlyClient->getCharge($referenceId);
        if (!$charge) {
            return null;
        }
        $chargeTransaction = (
            ChargeTransactions::where('reference_id', $referenceId)->first()
        );
        if (
            !$chargeTransaction
            || $chargeTransaction->status != ChargeTransactions::STATUS_PENDING
        ) {
            // QQQ recieved unexpected reference_id
            return null;
        }

        $status = $charge['data']['charge']['attributes']['status'];

        switch ($chargeTransaction->type) {
            case ChargeTransactions::TYPE_TOPUP:
                TopupBalance::dispatch($referenceId, $status);
                break;
            default:
                logger('Unexpected type '. $chargeTransaction->type);
        }
    }
}
