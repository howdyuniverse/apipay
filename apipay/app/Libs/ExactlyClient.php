<?php

declare(strict_types=1);

namespace App\Libs;

use GuzzleHttp\Client;

class ExactlyClient
{
    public const BASE_URL = 'https://api.exactly.com';
    public const STATUS_FAILED = 'failed';
    public const STATUS_SUCCESSFUL = 'successful';
    public const STATUS_PENDING = 'pending';
    public const STATUS_INITIALIZED = 'initialized';

    protected $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client([
            'base_uri' => ExactlyClient::BASE_URL,
        ]);
    }

    public static function create()
    {
        return new ExactlyClient();
    }

    public function post(string $apiMethod, array $params)
    {
        return $this->httpClient
            ->request('POST', $apiMethod, $params)->getBody();
    }

    public function get(string $apiMethod, array $params = [])
    {
        return $this->httpClient->request(
            'GET', $apiMethod, ['query' => $params]
        );
    }

    public function charges(array $params): ?array
    {
        // return $this->post('/api/charges', $params);
        return json_decode('
        {
            "data": {
                "charge": {
                    "type": "charge",
                    "id": "23f3d3b0-8dd3-11e9-b4d2-537b9f6d7fec",
                    "attributes": {
                        "livemode": true,
                        "status": "initialized",
                        "amount": "'. $params['amount'] .'",
                        "paid_amount": null,
                        "fee": null,
                        "rolling": null,
                        "total_amount": null,
                        "currency": "USD",
                        "pay_method": "'. $params['pay_method'] .'",
                        "description": "'. $params['description'] .'",
                        "secure_auth": null,
                        "source": {
                            "email": "'. $params['email'] .'"
                        },
                        "failure": null,
                        "reference_id": "'. $params['reference_id'] .'",
                        "created_at": 1560427359,
                        "updated_at": 1560427359,
                        "valid_till": 1560445359
                    }
                },
                "links": {
                    "redirect_uri": "https://endpoint.example.com"
                }
            },
            "meta": {
                "server_time": 1560427359,
                "server_timezone": "UTC",
                "api_version": "v2"
            }
        }
        ', true);
    }

    public function directCharges(array $params): ?array
    {
        // return $this->post('/api/direct/charges', $params);
        return json_decode('
        {
            "data": {
                "charge": {
                    "type": "charge",
                    "id": "d5d0e700-8dd4-11e9-a7d7-c30351fce0d6",
                    "attributes": {
                        "livemode": true,
                        "status": "successful",
                        "amount": "'. $params["amount"] .'",
                        "paid_amount": "'. $params["amount"] .'",
                        "fee": "0.01",
                        "rolling": "1.00",
                        "total_amount": "'. bcsub($params["amount"], '1', 2) .'",
                        "currency": "'. $params["currency"] .'",
                        "pay_method": "card",
                        "description": "Payment for invoice #00006",
                        "secure_auth": null,
                        "source": {
                            "email": "'. $params["email"] .'",
                            "ip_address": "'. $params["ip_address"] .'",
                            "country_code": "US",
                            "name": "'. $params["name"] .'",
                            "card_number": "'. $params["card_number"] .'",
                            "brand": "Visa",
                            "issuer_country_code": "US",
                            "issuer_name": "JPMORGAN CHASE BANK, N.A."
                        },
                        "failure": null,
                        "reference_id": "'. $params['reference_id'] .'",
                        "created_at": 1560428087,
                        "updated_at": 1560428087,
                        "valid_till": 1560446087
                    }
                }
            },
            "meta": {
                "server_time": 1560428087,
                "server_timezone": "UTC",
                "api_version": "v2"
            }
        }
        ', true);

        // return json_decode('
        // {
        //     "data": {
        //         "charge": {
        //             "type": "charge",
        //             "id": "7a936de0-8dd4-11e9-a2bb-6797c22e17b6",
        //             "attributes": {
        //                 "livemode": true,
        //                 "status": "pending",
        //                 "amount": "'. $params["amount"] .'",
        //                 "paid_amount": null,
        //                 "fee": null,
        //                 "rolling": null,
        //                 "total_amount": null,
        //                 "currency": "'. $params["currency"] .'",
        //                 "pay_method": "card",
        //                 "description": "Payment for invoice #00004",
        //                 "secure_auth": null,
        //                 "source": {
        //                     "email": "'. $params["email"] .'",
        //                     "ip_address": "'. $params["ip_address"] .'",
        //                     "country_code": "US",
        //                     "name": "'. $params["name"] .'",
        //                     "card_number": "'. $params["card_number"] .'",
        //                     "brand": "Visa",
        //                     "issuer_country_code": "US",
        //                     "issuer_name": "JPMORGAN CHASE BANK, N.A."
        //                 },
        //                 "failure": null,
        //                 "reference_id": "'. $params['reference_id'] .'",
        //                 "created_at": 1560427934,
        //                 "updated_at": 1560427934,
        //                 "valid_till": 1560445934
        //             }
        //         },
        //         "links": {
        //             "redirect_uri": "https://endpoint.example.com"
        //         }
        //     },
        //     "meta": {
        //         "server_time": 1560427934,
        //         "server_timezone": "UTC",
        //         "api_version": "v2"
        //     }
        // }
        // ', true);
    }

    public function getCharge(string $referenceId): ?array
    {
        // return $this->get('/api/charges/' . $referenceId);
        return json_decode('
        {
            "data": {
                "charge": {
                    "type": "successful",
                    "id": "7a936de0-8dd4-11e9-a2bb-6797c22e17b6",
                    "attributes": {
                        "livemode": true,
                        "status": "successful",
                        "amount": "1.00",
                        "paid_amount": null,
                        "fee": null,
                        "rolling": null,
                        "total_amount": null,
                        "currency": "USD",
                        "pay_method": "card",
                        "description": "Payment for invoice #000013",
                        "source": {
                            "email": "client@domain.ltd",
                            "ip_address": "198.51.100.1",
                            "country_code": "US",
                            "name": "John Doe",
                            "card_number": "411111******1111",
                            "brand": "Visa",
                            "issuer_country_code": "US",
                            "issuer_name": "JPMORGAN CHASE BANK, N.A."
                        },
                        "failure": {
                            "code": "processing_failed",
                            "message": "Authentication failed"
                        },
                        "reference_id": "'. $referenceId .'",
                        "created_at": 1487700048,
                        "updated_at": 1487700070,
                        "valid_till": 1487700648
                    }
                }
            },
            "meta": {
                "server_time": 1487761518,
                "server_timezone": "UTC",
                "api_version": "v2"
            }
        }
        ', true);
    }
}
