<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use JWTAuth;

use \App\Services\ScheduledPayService;
use \App\Http\Requests\ScheduledPayRequest;

class ScheduledPayController extends Controller
{
    public function __construct(ScheduledPayService $scheduledPayService)
    {
        $this->user = JWTAuth::parseToken()->authenticate();
        $this->schedulePayService = $scheduledPayService;
    }

    public function schedulePay(ScheduledPayRequest $request): JsonResponse
    {
        $errors = $request->validateRequestData($this->user);
        if (!empty($errors)) {
            return response()->json($errors, 400);
        }
        $scheduleData = $request->json()->all();
        $this->schedulePayService->schedulePayments($this->user, $scheduleData);
        return response()->json(['status' => 'ok'], 200);
    }
}
