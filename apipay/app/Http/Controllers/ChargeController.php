<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use JWTAuth;
use App\ChargeTransactions;
use App\Http\Requests\ChargeCallbackRequest;
use App\Http\Requests\TopupRequest;
use App\Http\Requests\TopupDirectRequest;
use App\Services\ChargeService;
use App\Apipay\Repositories\ChargeRepository;
use App\Apipay\Strategies\Charge\ChargeTransactionInitDirectStrategy;
use App\Apipay\Strategies\Charge\ChargeTransactionInitStrategy;

class ChargeController extends Controller
{
    protected $chargeService;
    protected $chargeRepo;

    public function __construct(
        ChargeService $chargeService,
        ChargeRepository $chargeRepo
    ) {
        $this->user = JWTAuth::parseToken()->authenticate();
        $this->chargeService = $chargeService;
        $this->chargeRepo = $chargeRepo;
    }

    public function topup(
        TopupRequest $request,
        ChargeTransactionInitStrategy $chargeStrategy
    )
    {
        $chargeState = $this->chargeService->initChargeTransaction(
            $this->user,
            $request->json()->all(),
            ChargeTransactions::TYPE_TOPUP,
            $chargeStrategy
        );
        return $chargeState->response();
    }

    public function topupDirect(
        TopupDirectRequest $request,
        ChargeTransactionInitDirectStrategy $chargeStrategy
    ) {
        $chargeState = $this->chargeService->initChargeTransaction(
            $this->user,
            ['client_ip' => $request->getClientIp()] + $request->json()->all(),
            ChargeTransactions::TYPE_TOPUP,
            $chargeStrategy
        );
        return $chargeState->response();
    }

    public function chargeCallbackHandler(ChargeCallbackRequest $request)
    {
        $this->chargeService->enqueueChargeCallbackEvent(
            $request->json()->all()
        );
        return response()->json(['status' => 'success'], 200);
    }

    public function getChargeTransaction($referenceId)
    {
        $charge = $this->chargeRepo->getChargeTransaction($referenceId);
        if (!$charge) {
            return response()->json(['status' => 'not found'], 404);
        }
        return response()->json(
            [
                'status' => $charge->status,
            ],
            200
        );
    }
}
