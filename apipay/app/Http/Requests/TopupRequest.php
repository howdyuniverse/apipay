<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseApiRequest;

class TopupRequest extends BaseApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|regex:/^[0-9]+(?:\.[0-9]{1,2})?$/',
            'reference_id' => 'required',
        ];
    }
}
