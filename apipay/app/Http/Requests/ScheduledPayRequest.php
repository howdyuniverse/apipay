<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Http\Requests\BaseApiRequest;

class ScheduledPayRequest extends BaseApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required|date_format:d/m/Y',
            'end_date' => 'required|date_format:d/m/Y',
            'description' => 'required|min:3|max:100',
            'recipients' => 'required|array',
            'recipients.*.id' => 'required|exists:users',
            'recipients.*.amount' => 'required|regex:/^[0-9]+(?:\.[0-9]{1,2})?$/',
        ];
    }

    public function validateRequestData($user)
    {
        $scheduleData = $this->json()->all();
        $recipients = $scheduleData['recipients'];
        $recipientsIds = [];
        foreach($recipients as $recipient) {
            $recipientsIds[] = $recipient['id'];
        }
        if (in_array($user->id, $recipientsIds)) {
            return [ 'errors' => ['sender_id in recipient_ids'] ];
        }
        return [];
    }
}
