<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseApiRequest;

class TopupDirectRequest extends BaseApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|regex:/^[0-9]+(?:\.[0-9]{1,2})?$/',
            'name' => 'required|min:3|max:100',
            'card_number' => 'required|integer|digits:16',
            'card_expiry_month' => 'required|integer|between:1,12',
            'card_expiry_year' => (
                'required|digits:4|integer|min:'
                . (date('Y')) .'|max:'. (date('Y') + 4)
            ),
            'card_csc' => 'required|integer|digits:3',
            'reference_id' => 'required',
        ];
    }
}
