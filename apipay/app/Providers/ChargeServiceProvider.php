<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ChargeService;
use App\Apipay\Repositories\ChargeRepository;
use App\Libs\ExactlyClient;

class ChargeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\ChargeService', function ($app) {
            return new ChargeService(
                new ChargeRepository(),
                new ExactlyClient(),
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
