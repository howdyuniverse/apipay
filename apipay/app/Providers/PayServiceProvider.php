<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Apipay\Repositories\PayRepository;
use App\Services\PayService;

class PayServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\PayService', function ($app) {
            return new PayService(new PayRepository());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
