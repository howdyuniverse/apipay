<?php

namespace App\Console\Commands;

use App\Services\ScheduledPayService;
use Illuminate\Console\Command;

class RegularPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:regular-payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'regular payments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ScheduledPayService $scheduledPayService)
    {
        parent::__construct();

        $this->scheduledPayService = $scheduledPayService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->scheduledPayService->processReadyScheduledTransactions();
    }
}
