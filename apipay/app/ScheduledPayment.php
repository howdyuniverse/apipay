<?php

namespace App;

use App\BaseModel;

class ScheduledPayment extends BaseModel
{
    protected $fillable = [
        'payment_schedule_id', 'pay_date', 'paid_at',
    ];

    public function paymentSchedule()
    {
        return $this->belongsTo(PaymentSchedule::class);
    }
}
