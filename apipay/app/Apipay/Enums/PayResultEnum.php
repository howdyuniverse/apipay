<?php

namespace App\Apipay\Enums;

class PayResultEnum
{
    public const PAYMENT_OK = 200;
    public const ERR_CODE_NOT_ENOUGH_MONEY = 403;
    public const ERR_CODE_INTERNAL = 500;
}
