<?php

declare(strict_types=1);

namespace App\Apipay\States\Charge;

use Illuminate\Http\JsonResponse;

abstract class ChargeState
{
    protected $data;
    protected $code;

    public function __construct(?array $data = null, ?int $code = null)
    {
        $this->data = $data;
        $this->code = $code;
    }
    
    abstract public function response(): JsonResponse;

    public static function get(?array $data = null, ?int $code = null)
    {
        return new static($data, $code);
    }
}
