<?php

declare(strict_types=1);

namespace App\Apipay\States\Charge;

use Illuminate\Http\JsonResponse;
use App\Apipay\States\Charge\ChargeState;

class ExistsChargeState extends ChargeState
{
    public function response(): JsonResponse
    {
        return response()->json(['status' => 'exists'], 409);
    }
}
