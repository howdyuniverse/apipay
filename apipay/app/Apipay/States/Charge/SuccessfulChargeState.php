<?php

declare(strict_types=1);

namespace App\Apipay\States\Charge;

use Illuminate\Http\JsonResponse;
use App\Apipay\States\Charge\ChargeState;

class SuccessfulChargeState extends ChargeState
{
    public function response(): JsonResponse
    {
        return response()->json(['status' => 'success'], 200);
    }
}
