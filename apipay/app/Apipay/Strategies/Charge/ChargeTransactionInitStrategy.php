<?php

declare(strict_types=1);

namespace App\Apipay\Strategies\Charge;

use App\User;
use App\ChargeTransactions;
use App\Libs\ExactlyClient;

use App\Apipay\Repositories\ChargeRepository;
use App\Apipay\States\Charge\ChargeState;
use App\Apipay\States\Charge\RedirectChargeState;
use App\Apipay\States\Charge\FailedChargeState;
use App\Apipay\States\Charge\UnexpectedChargeState;
use App\Apipay\States\Charge\ExistsChargeState;
use App\Apipay\Strategies\Charge\ChargeTransactionInitInterface;

class ChargeTransactionInitStrategy implements ChargeTransactionInitInterface
{
    protected $chargeRepo;
    protected $exactlyClient;

    public function __construct(
        ChargeRepository $chargeRepo,
        ExactlyClient $exactlyClient
    ) {
        $this->chargeRepo = $chargeRepo;
        $this->exactlyClient = $exactlyClient;
    }

    public function init(
        User $user,
        array $transactionData,
        string $type
    ): ChargeState {
        $referenceId = $transactionData['reference_id'];
        $amount = $transactionData['amount'];

        if (ChargeTransactions::where('reference_id', $referenceId)->exists()) {
            return ExistsChargeState::get();
        }

        $this->chargeRepo->createChargeTransaction(
            $user->id,
            $referenceId,
            $amount,
            ChargeTransactions::STATUS_RECIEVED,
            $type
        );

        $apiResponse = $this->exactlyClient->charges(
            [
                'amount' => $amount,
                'currency' => 'USD',
                'description' => 'topup for user id:' . $user->id,
                'email' => $user->email,
                'pay_method' => 'card',
                'reference_id' => $referenceId
            ]
        );
        // TODO check API HTTP response 400, 500 ...
        $responseAttrs = $apiResponse['data']['charge']['attributes'];
        $transactionStatus = $responseAttrs['status'];
        $this->chargeRepo->updateChargeTransactionStatus(
            $referenceId,
            ChargeTransactions::getModelStatusByApiStatus($transactionStatus),
        );

        $responseAttrs = $apiResponse['data']['charge']['attributes'];
        $transactionStatus = $responseAttrs['status'];

        switch ($transactionStatus) {
            case ExactlyClient::STATUS_INITIALIZED:
                $redirectUri = $apiResponse['data']['links']['redirect_uri'];
                return RedirectChargeState::get(['redirect_uri' => $redirectUri]);
            case ExactlyClient::STATUS_FAILED:
                return FailedChargeState::get();
        }

        return UnexpectedChargeState::get();
    }
}
