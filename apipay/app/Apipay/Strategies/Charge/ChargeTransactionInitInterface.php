<?php

declare(strict_types=1);

namespace App\Apipay\Strategies\Charge;

use App\Http\Requests\BaseApiRequest;
use App\User;
use App\Apipay\States\Charge\ChargeState;

interface ChargeTransactionInitInterface
{
    public function init(
        User $user, array $transactionData, string $type
    ): ChargeState;
}
