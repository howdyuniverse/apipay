<?php

declare(strict_types=1);

namespace App\Apipay\Strategies\Charge;

use App\User;
use App\ChargeTransactions;
use App\Libs\ExactlyClient;

use App\Apipay\Repositories\ChargeRepository;
use App\Apipay\States\Charge\ChargeState;
use App\Apipay\States\Charge\SuccessfulChargeState;
use App\Apipay\States\Charge\RedirectChargeState;
use App\Apipay\States\Charge\FailedChargeState;
use App\Apipay\States\Charge\UnexpectedChargeState;
use App\Apipay\States\Charge\ExistsChargeState;
use App\Apipay\Strategies\Charge\ChargeTransactionInitInterface;

class ChargeTransactionInitDirectStrategy implements ChargeTransactionInitInterface
{
    protected $chargeRepo;
    protected $exactlyClient;

    public function __construct(
        ChargeRepository $chargeRepo,
        ExactlyClient $exactlyClient
    ) {
        $this->chargeRepo = $chargeRepo;
        $this->exactlyClient = $exactlyClient;
    }

    public function init(
        User $user,
        array $transactionData,
        string $type
    ): ChargeState {
        $referenceId = $transactionData['reference_id'];
        if (ChargeTransactions::where('reference_id', $referenceId)->exists()) {
            return ExistsChargeState::get();
        }
        $amount = $transactionData['amount'];

        $this->chargeRepo->createChargeTransaction(
            $user->id,
            $referenceId,
            $amount,
            ChargeTransactions::STATUS_RECIEVED,
            $type
        );

        $apiResponse = $this->exactlyClient->directCharges(
            [
                'amount' => $amount,
                'currency' => 'USD',
                'description' => 'topup for user id:' . $user->id,
                'email' => $user->email,
                'pay_method' => 'card',
                'ip_address' => $transactionData['client_ip'],
                'name' => $transactionData['name'],
                'card_number' => $transactionData['card_number'],
                'card_expiry_month' => (
                    $transactionData['card_expiry_month']
                ),
                'card_expiry_year' => (
                    $transactionData['card_expiry_year']
                ),
                'card_csc' => $transactionData['card_csc'],
                'reference_id' => $transactionData['reference_id'],
            ]
        );
        $responseAttrs = $apiResponse['data']['charge']['attributes'];
        $transactionStatus = $responseAttrs['status'];
        $modelStatus =
            ChargeTransactions::getModelStatusByApiStatus($transactionStatus);
        if ($modelStatus == ChargeTransactions::STATUS_SUCCESSFUL) {
            // save successful charge with status pending
            // and process it later via API callback
            $modelStatus = ChargeTransactions::STATUS_PENDING;
        }
        $this->chargeRepo->updateChargeTransactionStatus(
            $referenceId,
            $modelStatus,
        );

        switch ($transactionStatus) {
            case ExactlyClient::STATUS_SUCCESSFUL:
                return SuccessfulChargeState::get();
            case ExactlyClient::STATUS_PENDING:
                $redirectUri = $apiResponse['data']['links']['redirect_uri'];
                return RedirectChargeState::get(['redirect_uri' => $redirectUri]);
            case ExactlyClient::STATUS_FAILED:
                return FailedChargeState::get();
        }
        return UnexpectedChargeState::get();
    }
}
