<?php

declare(strict_types=1);

namespace App\Apipay\Repositories;

use App\ChargeTransactions;

class ChargeRepository
{
    public function createChargeTransaction(
        int $userId,
        string $referenceId,
        string $amount,
        string $status,
        string $type
    ): ChargeTransactions {
        $chargeTransaction = ChargeTransactions::firstOrNew(
            ['reference_id' => $referenceId]
        );
        if ($chargeTransaction->exists) {
            return $chargeTransaction;
        }
        $nowDatetime = ChargeTransactions::getTimestampDatetime();
        $chargeTransaction->created_at = $nowDatetime;
        $chargeTransaction->updated_at = $nowDatetime;
        $chargeTransaction->user_id = $userId;
        $chargeTransaction->reference_id = $referenceId;
        $chargeTransaction->type = $type;
        $chargeTransaction->amount = $amount;
        $chargeTransaction->status = $status;
        $chargeTransaction->save();
        return $chargeTransaction;
    }

    public function updateChargeTransactionStatus(
        string $referenceId, string $newStatus
    ) {
        ChargeTransactions::where('reference_id', $referenceId)
            ->update([
                'status' => $newStatus,
                'updated_at' => ChargeTransactions::getTimestampDatetime()
            ]);
    }

    public function getChargeTransaction(string $referenceId)
    {
        return ChargeTransactions::where('reference_id', $referenceId)->first();
    }
}
